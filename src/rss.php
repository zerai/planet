<?php

use FeedIo\Factory;
use Innoscience\ComradeOPML\ComradeOPML;

function discoverFeed($site)
{
	static $exceptions = [];

	if (empty($exceptions) && file_exists('eccezioni.txt')) {
		$exceptions_file = file('eccezioni.txt', FILE_IGNORE_NEW_LINES);
		if ($exceptions_file != false) {
			foreach ($exceptions_file as $ex) {
				if (isset($ex[0]) && $ex [0] == '#')
					continue;
				$exceptions[] = trim($ex);
			}
		}
	}

	$feedIo = Factory::create(
        ['builder' => 'NullLogger'], // assuming you want feed-io to keep quiet
        ['builder' => 'GuzzleClient', 'config' => ['verify' => false]] // ignore SSL errors
    )->getFeedIo();
	echo "Cerca feed per $site\n";
	if ( (strpos($site,'facebook.com') !== false) ) {
        echo "Facebook quindi ignoro\n";
        return null;
	}
	
    $redirect_site = get_redirect( $site );
    if ( $redirect_site !== false ) {
        echo "URL redirect $redirect_site\n";
        $site = $redirect_site;
    }
        
    $feeds = $feedIo->discover($site);

	foreach ($feeds as $feed) {
		if (strncmp($feed, 'http', 4) != 0) {
			if (substr($feed, 0, 1) != '/') {
				$feed = '/' . $feed;
			}

			$parts = parse_url($site);
			$feed = sprintf('%s://%s%s', $parts['scheme'], $parts['host'], $feed);
		}

		if (in_array($feed, $exceptions)) {
			continue;
		}

		return $feed;
	}

	return null;
}

function retrieveFeeds()
{
	$elenco_regioni = [
		"abruzzo"    => "Abruzzo",
		"basilicata" => "Basilicata",
		"calabria"   => "Calabria",
		"campania"   => "Campania",
		"emilia"     => "Emilia Romagna",
		"friuli"     => "Friuli Venezia Giulia",
		"lazio"      => "Lazio",
		"liguria"    => "Liguria",
		"lombardia"  => "Lombardia",
		"marche"     => "Marche",
		"molise"     => "Molise",
		"piemonte"   => "Piemonte",
		"puglia"     => "Puglia",
		"sardegna"   => "Sardegna",
		"sicilia"    => "Sicilia",
		"toscana"    => "Toscana",
		"trentino"   => "Trentino Alto Adige",
		"umbria"     => "Umbria",
		"valle"      => "Valle d'Aosta",
		"veneto"     => "Veneto",
		"Italia"     => "Italia"
	];

	$ret = [];

	foreach ($elenco_regioni as $region => $name) {
		$lugs = file ('https://raw.github.com/Gelma/LugMap/master/db/' . $region . '.txt', FILE_IGNORE_NEW_LINES);

		foreach ($lugs as $lug) {
			try {
				list ($prov, $name, $zone, $site) = explode ('|', $lug);
				$feed = discoverFeed($site);

				if ($feed) {
					$ret[$name] = (object) [
						'xml' => $feed,
						'web' => $site,
					];
				}
			}
			catch(\Exception $e) {
				echo "Impossibile recuperare feed per " . $site . " su $feed: " . $e->getMessage() . "\n";
				continue;
			}
		}
	}

	ksort($ret);

	return $ret;
}

function generateLugOpml($filepath)
{
	$document = ComradeOPML::newDocument();

	foreach (retrieveFeeds() as $name => $url) {
		$document->addFeed('Main', $name, $url->xml, $name, $url->web);
	}

	ComradeOPML::exportFile($document, $filepath);
}

function generateIlsOpml($filepath)
{
    if ( ILSMANAGER_TOKEN === 'XXX' ) {
        echo "ILS manager token non definito\n";
        return null;
    }
    
	$contents = file_get_contents('https://ilsmanager.linux.it/ng/api/websites?token=' . urlencode(ILSMANAGER_TOKEN));

	$document = ComradeOPML::newDocument();

	foreach (explode("\n", $contents) as $row) {
		try {
			$row = trim($row);
			if (empty($row)) {
				continue;
			}

			list($name, $site) = explode(',', $row);

			$feed = discoverFeed($site);
			if ($feed) {
				$document->addFeed('Main', $name, $feed, $name, $site);
			}
		}
		catch(\Exception $e) {
			echo "Impossibile recuperare feed per " . $site . " su $name: " . $e->getMessage() . "\n";
		}
	}

	ComradeOPML::exportFile($document, $filepath);
} 

function retrieveContents($filepath, $limit)
{
	$feedIo = Factory::create(
        ['builder' => 'NullLogger'], // assuming you want feed-io to keep quiet
        ['builder' => 'GuzzleClient', 'config' => ['verify' => false]] // ignore SSL errors
    )->getFeedIo();
	$document = ComradeOPML::importFile($filepath);
	$items = [];

	foreach ($document->getAllCategories() as $category) {
		foreach ($category->getAllFeeds() as $feed) {
			try {
				$sitename = $feed->getText();

				$url = $feed->getXmlUrl();
				$result = $feedIo->read($url);
				foreach ($result->getFeed() as $item) {
					$item->setTitle(sprintf('%s - %s', $sitename, $item->getTitle()));
                    if(strlen($item->getContent()) > 400){
                        $desc = strip_tags($item->getContent(),'<p><br>');
                        $item->setContent(substr($desc,0,400) . '...');
                    }
					$items[] = $item;
				}
			}
			catch(\Exception $e) {
				echo "Impossibile leggere il feed di " . $feed->getTitle() . " su $url: " . $e->getMessage() . "\n";
			}
		}
	}

	$items = array_filter($items, function($i) {
		return !is_null($i->getLastModified()) && !empty($i->getTitle()) && !empty($i->getContent());
	});

	usort($items, function($first, $second) {
		return $first->getLastModified()->format('Y-m-d') <=> $second->getLastModified()->format('Y-m-d');
	});

	return array_slice(array_reverse($items), 0, $limit);
}

function formatPage($opml, $data, $outfile)
{
	$sorted = [];

	foreach($data as $i) {
		$date = $i->getLastModified()->format('d/m/y');

		if (!isset($sorted[$date])) {
			$sorted[$date] = [];
		}

		$sorted[$date][] = (object) [
			'title' => $i->getTitle(),
			'link' => $i->getLink(),
			'description' => $i->getContent(),
		];
	}

	$data = $sorted;

	$html = '';

	$html .= '<div class="row">';

	$html .= '<div class="col-8" style="overflow-x: hidden">';

	foreach($data as $date => $feeds) {
		$html .= '<div class="intro"><h2 class="m-0">' . $date . '</h2></div><hr>';

		foreach($feeds as $feed) {
			$html .= '<h4><a href="' . $feed->link . '">' . $feed->title . '</a></h4>';
			$html .= '<div>' . $feed->description . '</div><hr>';
		}
	}

	$html .= '</div>';

	$html .= '<div class="col-4">';
	$document = ComradeOPML::importFile($opml);

	$html .= '<a href="rss.xml"><img src="/images/feed-icon.png" alt="Planet"> Tutti i feed</a><br>';
	$html .= 'Contribuisci al planet su <a rel="nofollow" href="https://gitlab.com/ItalianLinuxSociety/Planet"><img src="https://www.linux.it/shared/index.php?f=immagini/gitlab.svg" style="height:20px" alt="Planet su GitLab"></a><br><br>';

	foreach ($document->getAllCategories() as $category) {
		foreach ($category->getAllFeeds() as $feed) {
			$url = $feed->getHtmlUrl();
			$name = $feed->getText();
			$html .= '<a href="' . $url . '"><img src="/images/feed-icon.png" alt="Feed ' . $name . '"> ' . $name . '</a><br>';
		}
	}

	$html .= '<br><br>I feed qui aggregati vengono automaticamente estratti dai siti indicizzati sulla <a href="http://lugmap.linux.it/">LugMap</a>';
	$html .= '</div>';

	$html .= '</div>';

	file_put_contents($outfile, $html);
}

function formatFeed($data, $title, $outfile)
{
	try {
		$feed = new FeedIo\Feed();
		$feed->addNS('atom', 'http://www.w3.org/2005/Atom');
		$feed->addNS('content', 'http://purl.org/rss/1.0/modules/content/');
		$feed->addNS('slash', 'http://purl.org/rss/1.0/modules/slash/');
		$feed->addNS('feedburner', 'http://rssnamespace.org/feedburner/ext/1.0');
		$feed->setTitle($title);
		$feed->setDescription('Feed generato da Planet.Linux.it');
		$feed->setLink('https://planet.linux.it/');

		foreach($data as $i) {
			$link = $i->getLink();
			$i->setPublicId($link);
			if (strpos($link, '&') !== false) {
				$i->setLink(substr($link, 0, strpos($link, '&')));
			}

			if ($i->getAuthor() && $i->getAuthor()->getName() == null) {
				$i->setAuthor(null);
			}

			if ($i->getAuthor() && strpos($i->getAuthor()->getName(), '@') === false) {
				$i->setAuthor(null);
			}


			$i->setPublicId(urlencode($i->getPublicId()));
			$feed->add($i);
		}

		$feedIo = Factory::create(
        ['builder' => 'NullLogger'], // assuming you want feed-io to keep quiet
        ['builder' => 'GuzzleClient', 'config' => ['verify' => false]] // ignore SSL errors
    )->getFeedIo();
		$xml = $feedIo->toRss($feed);
	}
	catch(\Exception $e) {
		echo "Impossibile formattare feed $title: " . $e->getMessage() . "\n";
		$xml = '';
	}
	
	$xml = str_replace('&nbsp;', '', $xml);
	$xml = str_replace('&hellip;', '', $xml);
    $xml = str_replace('aria-level="1"', '', $xml);
    $xml = preg_replace('/aria\-describedby=".*?"/i', '', $xml);
    $xml = preg_replace('/ style=".*?"/i', '', $xml);
	$xml = str_replace('<guid>', '<guid isPermaLink="false">', $xml);
    $xml = preg_replace('~<slash\:comments[^>]*>[^<]*</slash:comments>~', '', $xml);
    $xml = preg_replace('~<post\-id[^>]*>[^<]*</post\-id>~', '', $xml);
    $xml = preg_replace('~<wfw\:commentRss[^>]*>[^<]*</wfw\:commentRss>~', '', $xml);
    $xml = str_replace('<channel>', '<channel><atom:link href="https://planet.linux.it/lug/rss.xml" rel="self" type="application/rss+xml" />', $xml);
	$xml = str_replace("\n\n", '\n', $xml);
	$xml = str_replace(" ", '', $xml);

	file_put_contents($outfile, $xml);
}
